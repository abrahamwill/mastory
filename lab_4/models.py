from django.db import models

# Create your models here.
class Schedule(models.Model):
    namakegiatan = models.CharField(max_length=27)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=15)
    tanggal = models.DateTimeField()

class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, primary_key=True, unique=True)
    password = models.CharField(max_length=20)

    def __str__(self):
        return self.email
