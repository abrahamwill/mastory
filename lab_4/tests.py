# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve

# from .views import *
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time
# # Create your tests here.
# class Story8FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story8FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story8FunctionalTest, self).tearDown()

#     def test_title(self):
#         self.selenium.get('http://mastory.herokuapp.com/story8')
#         # self.browser.get('http://localhost:8000/Hello/')
#         # time.sleep(5)
#         self.assertIn("terbaek2017", self.selenium.title)

#     def test_header(self):
#         # self.browser.get('http://localhost:8000/Hello/')
#         self.selenium.get('http://mastory.herokuapp.com/story8')
#         header_text = self.selenium.find_element_by_tag_name('p').text
#         time.sleep(5)
#         self.assertIn("ABRAHAM WILLIAMS", header_text)

#     def test_header_with_css_property(self):
#         self.selenium.get('http://mastory.herokuapp.com/story8')
#         header_text = self.selenium.find_element_by_tag_name('p').value_of_css_property('font-size')
#         time.sleep(5)
#         self.assertIn('31px', header_text)
    
#     def test_night_mode(self):
#         self.selenium.get('http://mastory.herokuapp.com/story8')
#         time.sleep(5)
#         self.selenium.find_element_by_id('x').click()
#         bgcolor = self.selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
#         time.sleep(5)
#         self.assertIn('#252523',bgcolor)
    
#     def test_accordion(self):
#         self.selenium.get('http://mastory.herokuapp.com/story8')
#         headeraccordion = self.selenium.find_element_by_tag_name('h3').text
#         time.sleep(5)
#         self.assertIn("Aktivitas", headeraccordion)

# if __name__ == '__main__':
# 	unittest.main(warnings='ignore')

# class Story8TestApp(TestCase):
# 	def test_url_exist(self):
# 		response = Client().get('/story8')
# 		self.assertEqual(response.status_code, 301)

# 	def test_function_caller_exist(self):
# 		found = resolve('/story8')
# 		self.assertEqual(found.func, profile)

# 	'''def test_landingpage_containt(self):
# 		response = self.client.get('/story8')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Hello! I am', html_response)'''
