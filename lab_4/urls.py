from django.conf.urls import url
from django.urls import path

from .views import *

urlpatterns = [
    url(r'^$', profile, name='profile'),
    url(r'^page2$', page2, name='page2'),
    url(r'^registrasi$', registrasi, name='registrasi'),
    url(r'^newsched$', newsched, name='newsched'),
    url(r'^sched$', sched, name='sched'),
    url(r'^buku$', buku, name='buku'),
    # url(r'^subs$', subs, name='subs'),
    url(r'^schedtable$', schedtable, name='schedtable'),
    url(r'^deleteall$', deleteall, name='deleteall'),
    url(r'^subscribe$', subscribe, name='subscribe'),
    # url(r'^check_email$', check_email, name='check_email'),
    # url(r'^savesubs$', savesubs, name='savesubs'),
    path('check_email/',check_email, name='check_email'),
    path('savesubs/', savesubs, name='savesubs'),
    # path('unsubscribe/',unsubscribe, name='unsubscribe')
]
