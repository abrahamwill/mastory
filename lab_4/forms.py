from django import forms

class schedform(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    namakegiatan = forms.CharField(label='Nama Kegiatan', required=True, widget=forms.TextInput(attrs=attrs))
    tempat = forms.CharField(label='Tempat Kegiatan', required=True, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='Kategori', required=True,  widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateTimeField(label='Waktu Kegiatan',required=True, widget=forms.DateInput(attrs=attrs))

class subsform(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='name', required=True, widget=forms.TextInput(attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'Enter your full name'}))
    email = forms.EmailField(label='email', required=True, widget=forms.EmailInput(attrs={'id': 'email', 'class': 'form-control',
                                                            'placeholder': 'Enter your email'}))
    password = forms.CharField(label='password', required=True,  widget=forms.PasswordInput(attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your password'}))
