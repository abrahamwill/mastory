from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import *
from .models import *
from django.http import JsonResponse


# Create your views here.
def profile(request):
 # response = {'name' : mhs_name}
 return render(request,'profile.html')

def page2(request):
	return render(request, 'page2.html')

def registrasi(request):
	return render(request, 'registrasi.html')

def newsched(request):
	return render(request, 'newsched.html', {'schedform' : schedform})

def buku(request):
	return render(request, 'buku.html')

def subscribe(request):
    return render(request, 'subscribe.html', {'subsform' : subsform})

def check_email(request):
    if request.method == "POST":
        email = request.POST['email']
        check = Subscriber.objects.filter(email=email)
        if (check.exists()):
            return JsonResponse({'email_exists': True})
    return JsonResponse({'email_exists': False})


def savesubs(request):
    if request.method == "POST":
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        Subscriber.objects.create(name=name, email=email, password=password)
        if check_email:
            return JsonResponse({'is_success': True})
    return JsonResponse({'is_success': False})


def schedtable(request):
	form = schedform(request.POST)
	response = {}
	if (request.method == "POST"):
		response['namakegiatan'] = request.POST['namakegiatan']
		response['kategori'] = request.POST['kategori']
		response['tempat'] = request.POST['tempat']
		response['tanggal'] = request.POST['tanggal']
		schedule = Schedule(namakegiatan=response['namakegiatan'], kategori = response['kategori'],tempat=response['tempat'], tanggal=response['tanggal'])
		schedule.save()
		# return HttpResponseRedirect('/lab_4/sched/')
		response['schedule'] = Schedule.objects.all()
		return render(request, 'sched.html', response)
	else:
		return HttpResponseRedirect('')

# def subs(request):
#     subs = subs.objects.all()
#     return render(request, 'subscriber.html', {'subs' : subs})

def sched(request):
    schedule = Schedule.objects.all()
    return render(request, 'sched.html', {'schedule':schedule})

# def unsubscribe(request, email):
#     Subscriber.objects.filter(email=Email).delete()
#     return HttpResponseRedirect('subs')


def deleteall(request):
	Schedule.objects.all().delete()
	return HttpResponseRedirect('sched')
