from django import forms

class searchform(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    
    search = forms.CharField(label='search', required=True, widget=forms.TextInput(attrs=attrs))