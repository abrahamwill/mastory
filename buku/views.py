from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import requests
from .forms import searchform
from google.auth import credentials
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout

@csrf_exempt
def index(request):
    response={}

    response['searchform'] = searchform
    form = searchform(request.GET)
    global listbuku
    if form.is_valid():
        url = "https://www.googleapis.com/books/v1/volumes?q="
        url += request.GET['search']
        listbuku = requests.get(url).json()
        return render(request, 'buku.html', response)
    else:
        url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
        listbuku = requests.get(url).json()
        return render(request, 'buku.html', response)


    return render(request, 'buku.html', response)



def get_book(request):
    #listbuku['status_code'] = 200
    return JsonResponse(listbuku)

def login(request):
    if request.method == 'POST':
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(),'296928355554-jd4qkmovjfioqgjkc2k85co5jvi22tv0.apps.googleusercontent.com')
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['bukus'] = []

            return JsonResponse({'status': '0', 'url': reverse('index')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))

def home(request):
	return render(request, 'home.html')


#     response={}
#     response['searchform'] = searchform
#     form = searchform(request.POST)
#     listbuku = request.get(url).json
#     return render(request, 'buku.html', response)

# def tampil(request):
#     response={}
#     response['searchform'] = searchform
#     if (request.method == "POST"):
#         url = "https://www.googleapis.com/books/v1/volumes?q="
#         url += request.POST['search']
#         listbuku = request.get(url).json
#         return render(request, 'buku.html', response)
#         # return HttpResponseRedirect('/lab_4/sched/')
#     else:
#         url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
#         listbuku = request.get(url).json
#         return render(request, 'buku.html', response)

    # resp = req.json()

    # data_exist = False
    # stResponse = resp['Response']
    # print ("RESPONSE => ", stResponse)
    # if stResponse == "True":
    #     count_results = resp['totalResults']

    #     #cukup ambil 30 data saja
    #     cp = (int(count_results) / 10)
    #     if cp > 3: pages = 3
    #     elif cp > 0 and cp <= 3: pages = cp
    #     else: pages = 1
    #     data_exist = True

    # past_url = url
    # all_data = []
    # if data_exist:
    #     for page in range(pages):
    #         page += 1
    #         get_page = "&page="+str(page)
    #         new_url = past_url + get_page;
    #         new_req = requests.get(new_url).json()
    #         get_datas = new_req['Search']
    #         for data in get_datas:
    #             all_data.append(data)

    # return all_data
