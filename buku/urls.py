from django.urls import path
from .views import *
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('get-book/', get_book, name="get_book"),
    path('admin/', admin.site.urls),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),  # <- Here
    path('', index, name='home'),
]
