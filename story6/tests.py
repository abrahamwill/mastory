# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .models import Status
# from .forms import statform

# from .views import *
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time


# class Story6FunctionalTest(TestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		service_log_path = "./chromedriver.log"
# 		service_args = ['--verbose']
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(Story6FunctionalTest, self).setUp()
	
# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story6FunctionalTest, self).tearDown()
	
# 	def test_input_todo(self):
# 		selenium = self.selenium
# 		# Opening the link we want to testx
# 		selenium.get('http://mastory.herokuapp.com')
# 		time.sleep(5)
# 		# find the form element
# 		status = selenium.find_element_by_id('id_Status')
# 		hasil = selenium.find_element_by_name('liststatus').text
		
# 		submit = selenium.find_element_by_name('submit')
		
# 		# Fill the form with data
# 		status.send_keys('Coba Coba')
# 		time.sleep(5)
# 		# submitting the form
# 		submit.send_keys(Keys.RETURN)
# 		time.sleep(5)
# 		self.assertIn('Coba Coba', hasil)
		
# 	def test_title(self):
# 		self.selenium.get('http://mastory.herokuapp.com')
# 		# self.browser.get('http://localhost:8000/Hello/')
# 		# time.sleep(5)
# 		self.assertIn("terbaek2017", self.selenium.title)
	
# 	def test_header(self):
# 		# self.browser.get('http://localhost:8000/Hello/')
# 		self.selenium.get('http://mastory.herokuapp.com')
# 		header_text = self.selenium.find_element_by_tag_name('p').text
# 		time.sleep(5)
# 		self.assertIn("Hello apa kabar?", header_text)
	
# 	def test_header_with_css_property(self):
# 		self.selenium.get('http://mastory.herokuapp.com')
# 		header_text = self.selenium.find_element_by_tag_name('p').value_of_css_property('font-size')
# 		time.sleep(5)
# 		self.assertIn('50px', header_text)
	
# 	def test_response_header_with_css_property(self):
# 		self.selenium.get('http://mastory.herokuapp.com')
# 		header_text = self.selenium.find_element_by_name('liststatus').value_of_css_property('font-size')
# 		time.sleep(5)
# 		self.assertIn('30px', header_text)

# if __name__ == '__main__':
# 	unittest.main(warnings='ignore')

# class Story6TestApp(TestCase):
# 	def test_url_exist(self):
# 		response = Client().get('/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_function_caller_exist(self):
# 		found = resolve('/')
# 		self.assertEqual(found.func, story6)

# 	def test_landingpage_containt(self):
# 		response = self.client.get('')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Hello apa kabar?', html_response)

# 	def test_can_create_new_status(self):
# 		testText = 'Lorem Ipsum'
# 		new_act = Status.objects.create(Status= testText)
# 		counting_all_available_status =  Status.objects.all().count()
# 		self.assertEqual(counting_all_available_status, 1)

# 	def test_form_Status_rendered(self):
# 		# form = StatusForm()
# 		response = Client().get('/')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Status', html_response)

	
# 	def test_post_success_and_render_the_result(self):
# 		testText = 'Lorem Ipsum'
# 		# form_data = {'status':testText}
# 		response_post = Client().post('/', {'Status': testText})
# 		self.assertEqual(response_post.status_code, 302)
# 		# tmp = 		
# 		response= Client().get('/')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn(testText, html_response)

# 	def test_TextField_300char(self):
# 		testText = "?" * 301
# 		response = Client().post("/", {"Status": testText})
# 		# self.asserIsNotNone(stats)
# 		self.assertEqual(Status.objects.filter(Status = testText).count(), 0)
	
# 	def test_urlprofile_exist(self):
# 		response = Client().get('/profilechallenge')
# 		self.assertEqual(response.status_code, 200)
	
# 	def test_profilechallenge_containt(self):
# 		response = self.client.get('/profilechallenge')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Hello! I am', html_response)
	
# 	def test_profilechallengefunc_caller_exist(self):
# 		found = resolve('/profilechallenge')
# 		self.assertEqual(found.func, profilechallenge)
