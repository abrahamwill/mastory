from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', story6, name='story6'),
    url(r'^profilechallenge$',profilechallenge,name='profilechallenge'),
]