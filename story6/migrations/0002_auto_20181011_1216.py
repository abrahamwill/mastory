# Generated by Django 2.1.1 on 2018-10-11 05:16

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='dateTime',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='status',
            name='Status',
            field=models.CharField(max_length=300),
        ),
    ]
