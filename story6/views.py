from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import statform
from .models import Status

# Create your views here.
def story6(request):
    statuskita = Status.objects.all()
    # resp = {}
    if request.method == 'POST':
        form = statform(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = statform()
    return render(request, 'Story6.html', {'form':form, 'data':statuskita})

def profilechallenge(request):
 # response = {'name' : mhs_name}
 return render(request,'profilechallenge.html')


