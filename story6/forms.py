from django import forms
from .models import Status

class statform(forms.ModelForm):
    class Meta:
        model = Status

        fields = ('Status',)

        widgets = {
			'Status' : forms.Textarea(attrs={'class': 'form-control'}),
		}


