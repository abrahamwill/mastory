$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_is_available;
    var timer = 0;

    $('#email').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmailFormat, 1000);
        toggleButton();
    });

    $('#name').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#password').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#form').on('submit', function(event) {
        event.preventDefault();
        console.log("Form Submitted!");
        sendFormData();
    });

    function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "/check_email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function( ) {
                if (email.email_exists) {
                    email_is_available = false;
                    $('.errorlist').replaceWith("<p class='fail'>This email is already been used, please use another email!</p>");
                } else {
                    email_is_available = true;
                    toggleButton();
                }
            },
            error: function() {
                alert("Error, cannot validate email!")
            }
        })
    }

    function sendFormData() {
        $.ajax({
            method: 'POST',
            url: "/savesubs/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.errorlist p').replaceWith("<p class='success'>Data successfully saved!</p>");
                    console.log("Successfully add data");
                } else {
                    $('.errorlist p').replaceWith("<p class='fail'>Error! Data cannot be saved!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }

    function checkValidEmailFormat() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#email').val());

        if (is_valid) {
            validateEmail();
        } else {
            $('.errorlist p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }

    function toggleButton() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#email').val();
        if (password.length !== 0 && name.length !== 0 && email_is_available) {
            $('.errorlist p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name and password cannot be empty</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email and password cannot be empty</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name and email cannot be empty</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Password cannot be empty</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name cannot be empty</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email cannot be empty</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }
});

// Render Google Sign-in button
function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

// Sign-in success callback
function onSuccess(googleUser) {
    // Get the google profile data
    var profile = googleUser.getBasicProfile();

    // Get the google+ profile data
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            // Display the user details
            var profileHTML = '<h3>Welcome '+resp.name.givenName+'! <a href="javascript:void(0);" onclick="signOut();">Sign out</a></h3>';
            profileHTML += '<img src="'+resp.image.url+'"/><p><b>Google ID: </b>'+resp.id+'</p><p><b>Name: </b>'+resp.displayName+'</p><p><b>Email: </b>'+resp.emails[0].value+'</p><p><b>Gender: </b>'+resp.gender+'</p><p><b>Google Profile:</b> <a target="_blank" href="'+resp.url+'">click to view profile</a></p>';
            document.getElementsByClassName("userContent")[0].innerHTML = profileHTML;

            document.getElementById("gSignIn").style.display = "none";
            document.getElementsByClassName("userContent")[0].style.display = "block";
        });
    });
}

// Sign-in failure callback
// Render Google Sign-in button
function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

// Sign-in success callback
function onSuccess(googleUser) {
    // Get the google profile data
    var profile = googleUser.getBasicProfile();

    // Get the google+ profile data
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            // Display the user details
            var profileHTML = '<h3>Welcome '+resp.name.givenName+'! <a href="javascript:void(0);" onclick="signOut();">Sign out</a></h3>';
            profileHTML += '<img src="'+resp.image.url+'"/><p><b>Google ID: </b>'+resp.id+'</p><p><b>Name: </b>'+resp.displayName+'</p><p><b>Email: </b>'+resp.emails[0].value+'</p><p><b>Gender: </b>'+resp.gender+'</p><p><b>Google Profile:</b> <a target="_blank" href="'+resp.url+'">click to view profile</a></p>';
            document.getElementsByClassName("userContent")[0].innerHTML = profileHTML;

            document.getElementById("gSignIn").style.display = "none";
            document.getElementsByClassName("userContent")[0].style.display = "block";
        });
    });
}

// Sign-in failure callback
function onFailure(error) {
    alert(error);
}

// Sign out the user
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        document.getElementsByClassName("userContent")[0].innerHTML = '';
        document.getElementsByClassName("userContent")[0].style.display = "none";
        document.getElementById("gSignIn").style.display = "block";
    });

    auth2.disconnect();
}




// $(document).ready(function() {
//     $.get({
//       url: get_book_url,
//       context: document.body,
//       success: function(data) {
//         $.each(data['items'], function(i, item) {
//           $("#list-buku-html").append(
//             `
//             <tr>
//               <td class='text bg-grey'>
//                 <img width='50px' src='${ item["volumeInfo"]["imageLinks"]["smallThumbnail"] }' alt=''>
//               </td>
//               <td class="text bg-grey">${ item["volumeInfo"]["title"] }</td>
//               <td class="text bg-grey">${ item["volumeInfo"]["authors"][0] }</td>
//               <td class="text bg-grey">${ item["volumeInfo"]["publishedDate"] }</td>
//               <td class="text bg-grey">
//               <img class="fav" width="30" src= ` + starbefore + ` alt="" onclick="count(this)">
//             </td>
//             </tr>
//
//             `)
//         });
//       }
//     });
//   });
//
// var counter = 0;
// function count(caller) {
//       console.log("clicked")
//       if( $(caller).attr('src') == starafter) {
//           $(caller).attr('src', starbefore);
//           counter--;
//           $("#favorited").html(counter);
//       } else {
//           $(caller).attr('src', starafter);
//           counter++;
//           $("#favorited").html(counter);
//       }
//     }

// function myFunction() {
//     var myVar;
//     myVar = setTimeout(showPage, 3000);
// }
//
// function showPage() {
//   document.getElementById("loader").style.display = "none";
//   document.getElementById("allbooks").style.display = "block";
// }


//Subscribe
